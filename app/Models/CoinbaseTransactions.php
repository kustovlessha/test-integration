<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoinbaseTransactions extends Model
{
    use HasFactory;

    const STATUS_CONFIRMED = 'charge:confirmed';    // success status
    const STATUS_CREATED = 'charge:created';
    const STATUS_DELAYED = 'charge:delayed';
    const STATUS_FAILED = 'charge:failed';          // failed status
    const STATUS_PENDING = 'charge:pending';
    const STATUS_RESOLVED = 'charge:resolved';

    const FINAL_STATUSES = [
        self::STATUS_CONFIRMED,
        self::STATUS_FAILED,
    ];

    protected $fillable = [
        'seller_id',                // ID in Better World
        'customer_email',           // $arr["event"]["data"]["metadata"]["email"]
        'usdc_pricing',             // $arr["event"]["data"]["pricing"]["usdc"]
        'event_id',                 // $arr["event"]["id"]
        'event_type',               // $arr["event"]["type"]
        'resource',                 // $arr["event"]["data"]["resource"]
        'code',                     // $arr["event"]["data"]["code"]]
        'name',                     // $arr["event"]["data"]["name"]
        'coinbase_created_at',      // $arr["event"]["data"]["created_at"]
        'coinbase_expires_at',      // $arr["event"]["data"]["expires_at"]
        /*
            массив транзакций (со статусами)
            $lastElement = end($arr["event"]["data"]["timeline"]);
            $lastTransactionStatus = $lastElement["status"];
        */
        'timeline',
        /*
            фиксированая/не фиксированая
            $arr["event"]["data"]["pricing_type"]
        */
        'pricing_type',
        'coinbase_checkout_id',
    ];
}
