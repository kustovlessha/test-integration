<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Shop extends Model
{
    use HasFactory;

    const PHYSICAL_TYPE = 0;
    const ELECTRONIC_TYPE = 1;

    const STATUS_BLOCKED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_VACATION = 2;
    CONST STATUSES_AVAILABLE_FOR_CHANGE = [
        self::STATUS_ACTIVE, self::STATUS_VACATION,
    ];

    protected $fillable = [
        'user_id',
        'type',
        'status',
        'name',
        'logo',
        'description',
        'contacts',
    ];

    public static function getById($id)
    {
        return DB::table('shops')
            ->where('id', $id)
            ->first();
    }
}
