<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Search\Searchable;

class Item extends Model
{
    use HasFactory;
//    use Searchable;

    const STANDART_TYPE = 0;
    const ELECTRONIC_TYPE = 1;
    const STATUS_BLOCKED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUSES_AVAILABLE_FOR_CHANGE = [
        self::STATUS_ACTIVE, self::STATUS_INACTIVE,
    ];

    protected $fillable = [
        'shop_id',
        'type',
        'category_id',
        'name',
        'description',
        'logo',
        'usdc_price',
        'coinbase_checkout_id',
        'file_link',
        'status',
    ];

    public function scopeAvailable(Builder $query)
    {
        $query
            ->join('shops', 'items.shop_id', '=', 'shops.id')
            ->join('users', 'shops.user_id', '=', 'users.id')
            ->where('items.status', self::STATUS_ACTIVE)
            ->where('shops.status', Shop::STATUS_ACTIVE)
            ->where('users.status', User::STATUS_ACTIVE);
    }

    public function scopeFiltered(Builder $query, $maxPrice)
    {
        $minPriceFilter = request('filters.minPrice');
        $maxPriceFilter = request('filters.maxPrice');
        $filterCondition = ($minPriceFilter || $minPriceFilter === 0) || $maxPriceFilter;

        $query->when(
            $filterCondition,
            function (Builder $q) use ($maxPrice) {
                $q->whereBetween('usdc_price', [
                    request('filters.minPrice', 0),
                    request('filters.maxPrice', $maxPrice)
                ]);
            }
        );
    }

    public function scopeSorted(Builder $query)
    {
        $query->when(
            request('sort'),
            function (Builder $q) {
                switch (request('sort')) {
                    case 'name':
                        $q->orderBy('items.name', 'ASC');
                        break;
                    case "priceDown":
                        $q->orderBy('items.usdc_price', 'DESC');
                        break;
                    case "priceUp":
                        $q->orderBy('items.usdc_price', 'ASC');
                        break;
                }
            }
        );
    }

    public function scopeSearched(Builder $query)
    {
        $search = request('search') ? trim(request('search')) : null;

        $query->when(
            $search,
            function (Builder $q) use ($search) {
                $q->where(function ($query) use ($search) {
                    $query->where('items.name', 'like', "%{$search}%")
                        ->orWhere('items.description', 'like', "%{$search}%");
                });
            }
        );
    }
}
