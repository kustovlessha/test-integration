<?php

namespace App\Models\Clients;

use GuzzleHttp\Client;

class EtherscanClient
{
    const API_URL = "https://api.etherscan.io/api?";
    const MODULES = [
        "proxy" => "module=proxy&",
        "block" => "module=block&",
        "transaction" => "module=transaction&",
    ];
    const ACTIONS = [
        "transaction_by_hash" => "action=eth_getTransactionByHash&",
        "block_by_number" => "action=getblockreward&",
        "transaction_status" => "action=gettxreceiptstatus&",
    ];

    private $apiKey;
    private $client;

    public function __construct()
    {
        $this->apiKey = env('ETHERSCAN_API_KEY');
        $this->client = new Client();
    }

    public function getTransactionInfo(string $transactionHash): ?array
    {
        $resultData = ['transactionHash' => $transactionHash];
        $infoURL = self::API_URL . self::MODULES['proxy'] . self::ACTIONS['transaction_by_hash'];
        $infoURL .= "txhash=$transactionHash" . "&apikey=$this->apiKey";

        $response = $this->client->get($infoURL);
        $dataTransaction = json_decode($response->getBody(), true);

        if (isset($dataTransaction['result'])) {
            $result = $dataTransaction['result'];

            $value = $result['value'];  // Сумма транзакции в Wei
            $resultData['value'] = $this->weiToEth(hexdec($value));

            // Получение номера блока транзакции
            $blockNumber = hexdec($result['blockNumber']);

            $resultData['timestamp'] = $this->getTransactionDatatimeByBlockNumber($blockNumber);
            $resultData['walletFrom'] = $result['from'];
            $resultData['walletTo'] = $result['to'];
            $resultData['status'] = $this->getTransactionStatus($transactionHash);

            return $resultData;
        }

        return null;
    }

    public function getTransactionStatus(string $transactionHash): bool
    {
        $infoURL = self::API_URL . self::MODULES['transaction'] . self::ACTIONS['transaction_status'];
        $infoURL .= "txhash=$transactionHash" . "&apikey=$this->apiKey";

        $response = $this->client->get($infoURL);
        $data = json_decode($response->getBody(), true);

        if (
            isset($data['message'])
            && isset($data['result'])
            && $data['message'] == "OK"
            && $data['result']['status'] == 1
        ) {
            return true;
        }

        return false;
    }

    public function getBlockInfo(string $blockNumber): ?array
    {
        $infoURL = self::API_URL . self::MODULES['block'] . self::ACTIONS['block_by_number'];
        $infoURL .= "blockno=$blockNumber" . "&apikey=$this->apiKey";

        $response = $this->client->get($infoURL);
        $data = json_decode($response->getBody(), true);

        if (isset($data['result'])) {
            return $data['result'];
        }

        return null;
    }

    public function getTransactionDatatimeByBlockNumber(string $blockNumber): ?string
    {
        $block = $this->getBlockInfo($blockNumber);

        if (is_array($block)) {
            return $block['timeStamp'];
        }

        return null;
    }

    private function weiToEth($wei)
    {
        return $wei * 1000000000000000000;
    }
}
