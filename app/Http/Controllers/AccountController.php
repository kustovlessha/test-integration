<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Foundation\Application as FoundationApplication;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AccountController extends Controller
{
    public function index(): View|Application|Factory|FoundationApplication
    {
        $user = auth()->user();

        return view('account.index', compact('user'));
    }

    public function settings(): View|Application|Factory|FoundationApplication
    {
        $user = auth()->user();

        return view('account.settings', compact('user'));
    }

    public function updateUser(Request $request)
    {
        $user = auth()->user();

        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $user->id,
        ];
        $messages = [
            'name.required' => 'Данное поле - обязательное для заполнения.',
            'name.max' => 'Максимальная длина поля - 255 символов.',
            'email.required' => 'Пожалуйста, укажите адрес электронной почты.',
            'email.email' => 'Не корректный e-mail.',
            'email.max' => 'Слишком длинный e-mail.',
            'email.unique' => 'Данный e-mail уже занят.',
        ];

        $requestData = $request->all();

        $validator = Validator::make($requestData, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        // Обновляем данные пользователя
        $user->name = $requestData['name'];
        $user->email = $requestData['email'];

        $user->save();

        return response()->json(['success' => 'Данные пользователя успешно обновлены!']);
    }

    public function updateUserStatus(Request $request)
    {
        $user = auth()->user();

        if ($user->status === User::STATUS_BLOCKED) {
            return response()->json(['errors' => ['status' => ['Вы заблокированы. Обратитесь в службу поддержки.']]], 422);
        }

        $rules = [
            'status' => [
                'required',
                'integer',
                Rule::in(User::STATUSES_AVAILABLE_FOR_CHANGE),
            ],
        ];
        $messages = [
            'status.required' => 'Данное поле - обязательное для заполнения.',
            'status' => 'Не корректное значение.',
        ];

        $requestData = $request->all();

        $validator = Validator::make($requestData, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        // Обновляем данные пользователя
        $user->status = $requestData['status'];
        $user->save();

        return response()->json(['success' => 'Ваш статус успешно обновлен.']);
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:8|confirmed',
        ]);

        $user = Auth::user();

        // Проверка текущего пароля
        if (!Hash::check($request->input('current_password'), $user->password)) {
            return redirect()->back()->withErrors(['error' => 'Текущий пароль неверен']);
        }

        // Обновление пароля
        $user->update(['password' => Hash::make($request->input('new_password'))]);

        return redirect()->route('home')->with('success', 'Пароль успешно обновлен');
    }
}
