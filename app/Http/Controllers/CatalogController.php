<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hydrators\MainPageHydrator;
use App\Models\Item;
use App\Models\ItemCategory;

class CatalogController extends Controller
{
    private MainPageHydrator $mainPageHydrator;

    const ITEM_PER_PAGE = 20;

    public function __construct(MainPageHydrator $mainPageHydrator)
    {
        $this->mainPageHydrator = $mainPageHydrator;
    }

    public function index (Request $request, ItemCategory $category = null)
    {
        if (!$category) {
            return abort(404, 'Страница не найдена');
        }

        $sortField = request('sort');
        $search = request('search') ?? null;
        $categoryId = $category->id;
        $maxPrice = Item::max('usdc_price');

        $items = Item::query()
            ->select('items.*')
            ->where('category_id', '=', $category->id)
            ->available()
            ->filtered($maxPrice)
            ->sorted()
            ->searched()
            ->paginate(self::ITEM_PER_PAGE);

        $categories = $this->mainPageHydrator->buildCategoryTree(ItemCategory::all()->toArray());
        $filters = request('filters');

        return view('catalog.index', compact(
            'items', 'categories', 'categoryId', 'maxPrice', 'category', 'sortField', 'filters', 'search'
        ));
    }

    public function search(Request $request)
    {
        return null;
    }
}
