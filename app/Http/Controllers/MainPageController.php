<?php

namespace App\Http\Controllers;

use App\Hydrators\MainPageHydrator;
use App\Models\Item;
use App\Models\ItemCategory;

class MainPageController extends Controller
{

    private MainPageHydrator $mainPageHydrator;

    const ITEM_PER_PAGE = 20;

    public function __construct(MainPageHydrator $mainPageHydrator)
    {
        $this->mainPageHydrator = $mainPageHydrator;
    }

    public function index ()
    {
        $items = Item::query()->limit(self::ITEM_PER_PAGE)->get();
        $categories = $this->mainPageHydrator->buildCategoryTree(ItemCategory::all()->toArray());

        return view('main-page.index', compact('items', 'categories'));
    }
}
