<?php

namespace App\Http\Controllers;

use App\Helpers\UrlHelper;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class ShopController extends Controller
{
    public function index()
    {
        $shops = Shop::where('user_id', Auth::id())->get()->all();
        return view('shops.index', compact('shops'));
    }

    public function create()
    {
        return view('shops.create');
    }

    public function store(Request $request)
    {
        $rules = [
//            'type' => 'required|in:0,1',
            'status' => [
                'required',
                'integer',
                Rule::in(Shop::STATUSES_AVAILABLE_FOR_CHANGE),
            ],
            'name' => 'required|string|max:255',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
            'description' => 'nullable|string',
            'contacts' => 'nullable|string',
        ];
        $messages = [
//            'type.required' => 'Данное поле - обязательное для заполнения.',
//            'type.in' => 'Не корректный тип магазина.',
            'status.required' => 'Данное поле - обязательное для заполнения.',
            'status' => 'Не корректный статус магазина.',
            'name.required' => 'Данное поле - обязательное для заполнения.',
            'name.max' => 'Слишком длинное название магазина.',
            'logo.image' => 'Данный файл не является изображением.',
            'logo.mimes' => 'Допустимы лишь следующие форматы изображения: jpeg,png,jpg.',
            'logo.max' => 'Изображение должно быть не более 1MB.',
        ];

        $requestData = $request->all();

        $validator = Validator::make($requestData, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $requestData['user_id'] = Auth::id();
        $requestData['type'] = 1;
        $requestData['logo'] = "/app-images/shop-logo.jpg";

        // Save logo image
        if ($request->hasFile('logo')) {
            $logoFile = $request->file('logo');
            $userId = Auth::id();
            $fileName = time() . "-" . $userId . "." . $logoFile->getClientOriginalExtension();
            $folder = UrlHelper::makePathForImage();

            if($logoFile->move(public_path($folder), $fileName)) {
                $requestData['logo'] = "/" . $folder . "/" . $fileName;
            }
        }

        Shop::create($requestData);
        Session::flash('success', 'Магазин успешно создан');

        return response()->json(['success' => 'Магазин успешно создан'], 200);
    }

    public function show(Shop $shop)
    {
        return redirect()->route('shops.index');
    }

    public function edit(Shop $shop)
    {
        return view('shops.edit', compact('shop'));
    }

    public function update(Request $request, Shop $shop)
    {
        $oldLogo = trim($shop->logo, "/");

        $rules = [
            'status' => [
                'required',
                'integer',
                Rule::in(Shop::STATUSES_AVAILABLE_FOR_CHANGE),
            ],
            'name' => 'required|string|max:255',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
            'description' => 'nullable|string',
            'contacts' => 'nullable|string',
        ];
        $messages = [
            'status.required' => 'Данное поле - обязательное для заполнения.',
            'status' => 'Не корректный статус магазина.',
            'name.required' => 'Данное поле - обязательное для заполнения.',
            'name.max' => 'Слишком длинное название магазина.',
            'logo.image' => 'Данный файл не является изображением.',
            'logo.mimes' => 'Допустимы лишь следующие форматы изображения: jpeg,png,jpg.',
            'logo.max' => 'Изображение должно быть не более 1MB.',
        ];

        $requestData = $request->all();

        if ($shop->status == 3) {
            return response()->json(['errors' => ['status' => [0 => 'Магазин заблокирован, его редактировать нельзя']]], 422);
        }

        $validator = Validator::make($requestData, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        // Save logo image
        if ($request->hasFile('logo')) {
            $logoFile = $request->file('logo');
            $userId = Auth::id();
            $fileName = time() . "-" . $userId . "." . $logoFile->getClientOriginalExtension();
            $folder = UrlHelper::makePathForImage();

            if($logoFile->move(public_path($folder), $fileName)) {
                $requestData['logo'] = "/" . $folder . "/" . $fileName;

                // Remove old logo
                if (File::exists($oldLogo) && !UrlHelper::isDefaultLogo($oldLogo)) {
                    File::delete($oldLogo);
                }
            }
        }

        $shop->update($requestData);

        return response()->json(['message' => 'Магазин успешно обновлен'], 200);
    }

    public function destroy(Shop $shop)
    {
        $shop->delete();

        return redirect()->route('shops.index')->with('success', 'Магазин успешно удален');
    }
}
