<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Shop;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ItemsController extends Controller
{
    public function checkById(Request $request): JsonResponse
    {
        $rules = [
            'itemId' => 'required|integer',
        ];
        $messages = [
            'itemId' => 'Not valid item ID.',
        ];

        $requestData = $request->all();

        $validator = Validator::make($requestData, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $itemId = $request->input('itemId');
        $item = Item::where('id', $itemId)
            ->select("id", "shop_id", "type", "name", "usdc_price", "status", "created_at")
            ->first();

        return response()->json(['item' => $item]);
    }

    public function checkItemsActivity(Request $request): JsonResponse
    {
        $requestData = $request->all();

        if (
            !is_array($requestData)
            || !array_key_exists("itemsListIds", $requestData)
            || empty($requestData['itemsListIds'])
        ) {
            return response()->json(['errors' => "Not validate request data."], 422);
        }

        $itemsListIds = $requestData['itemsListIds'];
        foreach ($itemsListIds as $itemId) {
            if (!is_numeric($itemId)) {
                return response()->json(['errors' => "Not validate request data."], 422);
            }
        }

        $items = Item::whereIn('items.id', $itemsListIds)
            ->select(
                'items.id as itemId',
                'items.status as itemStatus',
                'items.usdc_price as itemUsdcPrice',
                'shops.status as shopStatus',
                'users.status as userStatus'
            )
            ->available()
            ->get()
            ->toArray();

        return response()->json(['items' => $items]);
    }
}
