<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ShopController extends Controller
{
    public function checkById(Request $request): JsonResponse
    {
        $rules = [
            'shopId' => 'required|integer',
        ];
        $messages = [
            'shopId' => 'Not valid shop ID.',
        ];

        $requestData = $request->all();

        $validator = Validator::make($requestData, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $shopId = $request->input('shopId');
        $shop = Shop::where('id', $shopId)
            ->select("id", "user_id", "type", "status", "name", "created_at")
            ->first();

        return response()->json(['shop' => $shop]);
    }
}
