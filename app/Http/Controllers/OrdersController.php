<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrdersController extends Controller
{
    public function create(Request $request)
    {
        $rules = [
            'itemId' => 'required|integer',
            'email' => 'required|email|max:255',
        ];
        $messages = [
            'itemId.integer' => 'Не корректный id товара.',
            'itemId.required' => 'Не указан id товара.',
            'email.required' => 'Пожалуйста, укажите адрес электронной почты.',
            'email.email' => 'Не корректный e-mail.',
            'email.max' => 'Слишком длинный e-mail.',
        ];

        $requestData = $request->all();
        $validator = Validator::make($requestData, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $item = Item::where('id', $requestData['itemId'])->first();

        if (!$item) {
            return response()->json(['error' => ['itemId' => 'Товара не существует.']], 422);
        }

        $order = Order::where('customer_email', $requestData['email'])
            ->where('item_id', $requestData['itemId'])
            ->first();

        if (!$order) {
            $order = new Order;
            $order->customer_email = trim($requestData['email']);
            $order->item_id = $requestData['itemId'];
            $order->status = Order::STATUS_NEW;
            $order->shop_id = $item->shop_id;
            $order->usdc_price = $item->usdc_price;

            if (!$order->save()) {
                return response()->json(['error' => 'Can not create new order'], 422);
            }
        }

        return response()->json(['order_id' => $order->id], 200);
    }

    public function checkPayment(Request $request)
    {
        $rules = [
            'orderId' => 'required|integer',
        ];
        $messages = [
            'orderId.integer' => 'Не корректный номер заказа.',
            'orderId.required' => 'Не указан номер заказа.',
        ];

        $requestData = $request->all();
        $validator = Validator::make($requestData, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $order = Order::where('id', $requestData['orderId'])->first();
        $failLink = null;

        if (!$order) {
            return response()->json(['error' => 'Order is not exists'], 422);
        }

        if ($order->status == Order::STATUS_SUCCESS) {
            $item = Item::where('id', $order->item_id)->first();

            if ($item) {
                $failLink = $item->file_link;
            }
        }

        return response()->json([
            'orderStatus' => $order->status,
            'fileLink' => $failLink
        ], 200);
    }
}
