<?php

namespace App\Http\Controllers\Crypto;

use App\Http\Controllers\Controller;
use App\Models\Clients\EtherscanClient;
use Illuminate\Http\Request;

class EtherscanController extends Controller
{
    public function checkTransaction()
    {
        /*
            ** 1. Кошелёк отправителя
            ** 2. Кошелёк получателя
         * 3. Монета
         * 4. Сумма
         * 5. Еквивалент суммы в долларах
            ** 6. Статус транзакции
            ** 7. Дата и время транзакции в UTC
            ** 8. transactionHash
         * */

        $transactionHash = [
            "success" => "0x0abbd449688c23c1f192ff9648e287fb27b9ce5a6afa756c8cd386990ec8f2e4",
            "success2" => "0x8839103b16dcc0c740b54dea1abf226b48f2cfe4951ef39a69ae2213e28ff75a",
            "error" => "0x03aafec524ee85a6dc88cd4627dd25312a2634f89f5342f0ed6e23384aca22b1",
        ];

        $client = new EtherscanClient();

        dd($client->getTransactionInfo($transactionHash['success2']));

        //return $client->getTransactionInfo($transactionHash['success2']);
    }
}
