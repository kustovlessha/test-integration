<?php

namespace App\Http\Controllers\Crypto;

use App\Http\Controllers\Controller;
use App\Models\CoinbaseTransactions;
use App\Models\Item;
use App\Models\Order;
use App\Models\Webhook;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CoinbaseController extends Controller
{
    public function index()
    {
        return null;
    }

    // Webhook Coinbase Commerce
    public function coinbasePaymentNotifications(Request $request): JsonResponse
    {
        $requestData = $request->post();
        $coinbaseTransactions = new CoinbaseTransactions();
        $coinbaseTransactions->seller_id = $request->get('sel') ?? 0;

        if (
            !empty($requestData)
            && is_array($requestData)
            && array_key_exists('event', $requestData)
            && is_array($requestData['event'])
        ) {
            $event = $requestData['event'];
            $coinbaseTransactions->event_id = array_key_exists('id', $event) ? $event['id'] : 0;
            $coinbaseTransactions->event_type = array_key_exists('type', $event) ? $event['type'] : "";
            $coinbaseTransactions->usdc_pricing = $event["data"]["pricing"]["usdc"]["amount"];
            $coinbaseTransactions->coinbase_checkout_id = $event["data"]["checkout"]["id"];

            if (array_key_exists('data', $event) && is_array($event['data'])) {
                $eventData= $event['data'];
                $coinbaseTransactions->resource = array_key_exists('resource', $eventData) ? $eventData['resource'] : "";
                $coinbaseTransactions->code = array_key_exists('code', $eventData) ? $eventData['code'] : "";
                $coinbaseTransactions->name = array_key_exists('name', $eventData) ? $eventData['name'] : "";
                $coinbaseTransactions->coinbase_created_at = array_key_exists('created_at', $eventData) ? $eventData['created_at'] : "";
                $coinbaseTransactions->coinbase_expires_at = array_key_exists('expires_at', $eventData) ? $eventData['expires_at'] : "";
                $coinbaseTransactions->timeline = array_key_exists('timeline', $eventData) ? json_encode($eventData['timeline']) : "[]";
                $coinbaseTransactions->pricing_type = array_key_exists('pricing_type', $eventData) ? $eventData['pricing_type'] : "";
            }

            if (isset($event["data"]["metadata"]) && array_key_exists("email", $event["data"]["metadata"])) {
                $coinbaseTransactions->customer_email = trim($event["data"]["metadata"]["email"]);
            } else {
                $coinbaseTransactions->customer_email = 0;
            }
        }

        $coinbaseTransactions->save();

        if (in_array($coinbaseTransactions->event_type, CoinbaseTransactions::FINAL_STATUSES)) {
            $checkoutId = $coinbaseTransactions->coinbase_checkout_id;
            $item = Item::where('coinbase_checkout_id', $checkoutId)->first();

            if ($item) {
                $email = $coinbaseTransactions->customer_email ? trim($coinbaseTransactions->customer_email) : '';
                $order = Order::where('item_id', $item->id)
                    ->where('customer_email', $email)
                    ->where('status', Order::STATUS_NEW)
                    ->first();
                $orderStatus = null;

                if ($coinbaseTransactions->event_type == CoinbaseTransactions::STATUS_CONFIRMED) {
                    $orderStatus = Order::STATUS_SUCCESS;
                } elseif ($coinbaseTransactions->event_type == CoinbaseTransactions::STATUS_FAILED) {
                    $orderStatus = Order::STATUS_FAIL;
                }

                if (
                    $order
                    && $orderStatus
                    && $coinbaseTransactions->usdc_pricing == $order->usdc_price
                    && $coinbaseTransactions->seller_id == $order->shop_id
                ) {
                    $order->status = $orderStatus;
                    $order->save();
                }
            }
        }

        return response()->json($request);
    }
}
