<?php

namespace App\Http\Controllers;

use App\Helpers\UrlHelper;
use App\Models\Item;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class SellerItemsController extends Controller
{
    public function index($shopId)
    {
        $shop = Shop::getById($shopId);

        if (!$shop || Auth::id() !== $shop->user_id) {
            abort(404);
        }

        $items = Item::where('shop_id', $shop->id)->get()->toArray();

        return view('seller-items.index', compact('items', 'shop'));
    }

    public function create($shopId)
    {
        $shop = Shop::getById($shopId);

        if (!$shop || Auth::id() !== $shop->user_id) {
            abort(404);
        }

        return view('seller-items.create', compact('shop'));
    }

    public function store(Request $request)
    {
        $requestData = $request->all();

        if (empty($requestData['type'])) {
            return response()->json(['errors' => [
                'type' => 'Empty or not correct item type',
            ]], 422);
        }

        if ($requestData['type'] == Item::ELECTRONIC_TYPE) {
            // electronic item rules
            $rules = [
                'shop_id' => 'required|numeric',
                'type' => 'required|in:0,1',
                'category_id' => 'required|numeric',
                'name' => 'required|string|max:255',
                'usdc_price' => ['required', 'numeric', 'regex:/^\d+(\.\d{1,15})?$/'],
                'logo' => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
                'coinbase_checkout_id' => 'required|string',
                'file_link' => 'required|string',
                'description' => 'nullable|string',
            ];
            $messages = [
                'shop_id.required' => 'Данное поле - обязательное для заполнения.',
                'shop_id.numeric' => 'Данное поле - должно быть целым числом.',
                'type.required' => 'Данное поле - обязательное для заполнения.',
                'type.in' => 'Не корректный тип товара.',
                'category_id.required' => 'Данное поле - обязательное для заполнения.',
                'category_id.numeric' => 'Данное поле - должно быть целым числом.',
                'name.required' => 'Данное поле - обязательное для заполнения.',
                'name.max' => 'Слишком длинное название магазина.',
                'usdc_price.required' => 'Данное поле - обязательное для заполнения.',
                'usdc_price.regex' => 'Данное поле - должно быть числом.',
                'logo.image' => 'Данный файл не является изображением.',
                'logo.mimes' => 'Допустимы лишь следующие форматы изображения: jpeg,png,jpg.',
                'logo.max' => 'Изображение должно быть не более 1MB.',
                'coinbase_checkout_id' => 'Данное поле - обязательное для заполнения.',
                'file_link' => 'Данное поле - обязательное для заполнения.',
            ];
        } else {
            // standart item rules
            $rules = [];
            $messages = [];
        }

        $validator = Validator::make($requestData, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $shop = Shop::getById($requestData['shop_id']);
        $userId =  Auth::id();

        if (empty($shop) || $userId != $shop->user_id) {
            return response()->json(['errors' => ['shop_id' => 'Данный магазин не найден.']], 422);
        }

        $requestData['logo'] = "/app-images/electronic-item-logo.jpg";

        // Save logo image
        if ($request->hasFile('logo')) {
            $logoFile = $request->file('logo');
            $fileName = time() . "-" . $userId . "." . $logoFile->getClientOriginalExtension();
            $folder = UrlHelper::makePathForImage();

            if($logoFile->move(public_path($folder), $fileName)) {
                $requestData['logo'] = "/" . $folder . "/" . $fileName;
            }
        }

        Item::create($requestData);
        Session::flash('success', 'Товар успешно создан');

        return response()->json(['message' => 'Товар успешно создан'], 200);
    }

    public function edit(Item $item)
    {
        return view('seller-items.edit', compact('item'));
    }

    public function update(Request $request, Item $item)
    {
        $requestData = $request->all();
        $oldLogo = trim($item->logo, "/");

        if ($item->type == Item::ELECTRONIC_TYPE) {
            // electronic item rules
            $rules = [
                'category_id' => 'required|numeric',
                'name' => 'required|string|max:255',
                'usdc_price' => ['required', 'numeric', 'regex:/^\d+(\.\d{1,15})?$/'],
                'logo' => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
                'coinbase_checkout_id' => 'required|string',
                'file_link' => 'required|string',
                'description' => 'nullable|string',
                'status' => [
                    'required',
                    'integer',
                    Rule::in(Item::STATUSES_AVAILABLE_FOR_CHANGE),
                ],
            ];
            $messages = [
                'category_id.required' => 'Данное поле - обязательное для заполнения.',
                'category_id.numeric' => 'Данное поле - должно быть целым числом.',
                'name.required' => 'Данное поле - обязательное для заполнения.',
                'name.max' => 'Слишком длинное название магазина.',
                'usdc_price.required' => 'Данное поле - обязательное для заполнения.',
                'usdc_price.regex' => 'Данное поле - должно быть числом.',
                'logo.image' => 'Данный файл не является изображением.',
                'logo.mimes' => 'Допустимы лишь следующие форматы изображения: jpeg,png,jpg.',
                'logo.max' => 'Изображение должно быть не более 1MB.',
                'coinbase_checkout_id' => 'Данное поле - обязательное для заполнения.',
                'file_link' => 'Данное поле - обязательное для заполнения.',
                'status.required' => 'Данное поле - обязательное для заполнения.',
                'status' => 'Не корректный статус товара.',
            ];
        } else {
            // standart item rules
            $rules = [];
            $messages = [];
        }

        $validator = Validator::make($requestData, $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $shop = Shop::getById($item->shop_id);
        $userId =  Auth::id();

        if (empty($shop) || $userId != $shop->user_id) {
            return response()->json(['errors' => ['shop_id' => 'Данный магазин не найден.']], 422);
        }

        // Save logo image
        if ($request->hasFile('logo')) {
            $logoFile = $request->file('logo');
            $fileName = time() . "-" . $userId . "." . $logoFile->getClientOriginalExtension();
            $folder = UrlHelper::makePathForImage();

            if($logoFile->move(public_path($folder), $fileName)) {
                $requestData['logo'] = "/" . $folder . "/" . $fileName;

                // Remove old logo
                if (File::exists($oldLogo) && !UrlHelper::isDefaultLogo($oldLogo)) {
                    File::delete($oldLogo);
                }
            }
        }

        $item->update($requestData);

        return response()->json(['message' => 'Товар успешно обновлен'], 200);
    }

    public function destroy(Item $item)
    {
        $shopId = $item->shop_id;
        $item->delete();

        return redirect()->route('seller.items.index', $shopId)->with('success', 'Товар успешно удален');
    }
}
