<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Foundation\Application as FoundationApplication;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    public function show(Item $item): View|Application|Factory|FoundationApplication
    {
        return view('items.show', compact('item'));
    }
}
