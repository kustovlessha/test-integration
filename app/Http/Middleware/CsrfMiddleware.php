<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CsrfMiddleware
{
    /**
     * Обработка входящего запроса.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Проверка CSRF только для POST-запросов
        if ($request->isMethod('post')) {
            $this->checkCsrf($request);
        }

        return $next($request);
    }

    /**
     * Проверка CSRF-токена.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function checkCsrf(Request $request)
    {
        $token = $request->input('_token') ?: $request->header('X-CSRF-TOKEN');

        if (! $token || ! hash_equals($request->session()->token(), $token)) {
            abort(419, 'Page Expired');
        }
    }
}
