<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;

class UrlHelper
{
    const ITEMS_COUNT_IN_FOLDER = 1000;
    const DEFAULT_IMAGES_FOLDER = "app-images";

    public static function makePathForImage (): string
    {
        $directory = public_path('images');
        $currentFolder = self::currentFolder($directory);

        if (($currentFolder === null)) {
            return 'images/1/1/1';
        }

        $currentFolderString = $directory . "/" . $currentFolder[0] . "/" . $currentFolder[1] . "/" . $currentFolder[2];
        $countFilesInCurrentFolder = self::countFiles($currentFolderString);

        if (!$countFilesInCurrentFolder || $countFilesInCurrentFolder >= self::ITEMS_COUNT_IN_FOLDER) {
            return self::newFolderPath($currentFolder, 'images');
        }

        return 'images/' . $currentFolder[0] . "/" . $currentFolder[1] . "/" . $currentFolder[2];
    }

    public static function newFolderPath(array $currentFolder, string $baseFolderName): string
    {
        if ($currentFolder[2] <= self::ITEMS_COUNT_IN_FOLDER) {
            $currentFolder[2]++;

            return $baseFolderName . "/" . $currentFolder[0] . "/" . $currentFolder[1] . "/" . $currentFolder[2];
        } else {
            $currentFolder[2] = 1;
            $currentFolder[1]++;
        }

        if ($currentFolder[1] <= self::ITEMS_COUNT_IN_FOLDER) {
            return $baseFolderName . "/" . $currentFolder[0] . "/" . $currentFolder[1] . "/" . $currentFolder[2];
        } else {
            $currentFolder[1] = 1;
            $currentFolder[0]++;
        }

        return $baseFolderName . "/" . $currentFolder[0] . "/" . $currentFolder[1] . "/" . $currentFolder[2];
    }

    /**
     * Возвращает трехмерную вложеность в файловой системе
     * null - переданой директории не существует
     * [номер директории первой линии, номер директории второй линии, номер директории третьей линии]
     */
    public static function currentFolder($directory): ?array
    {
        $result = [];

        // images folder
        $firstLine = self::countFolders($directory);
        if ($firstLine === null) {
            return null;
        }

        // first line folder
        $result[] = $firstLine;
        $secondLine = self::countFolders($directory . "/" . $firstLine);

        if ($secondLine === null) {
            $result[] = 1;
            $result[] = 1;

            return $result;
        }

        // second line folder
        $result[] = $secondLine;
        $threadLine = self::countFolders($directory . "/" . $firstLine . "/" . $secondLine);

        if ($threadLine === null) {
            $result[] = 1;
            return $result;
        }

        // thread line folder
        $result[] = $threadLine;

        return $result;
    }

    /**
     * null - $directory is not exists
     * int - folders count in $directory
     */
    public static function countFolders($directory): ?int
    {
        // Проверяем, существует ли директория
        if (File::exists($directory) && is_dir($directory)) {
            $subdirectories = File::directories($directory);

            return count($subdirectories);
        }

        return null;
    }

    public static function countFiles($directory): ?int
    {
        // Проверяем, существует ли директория
        if (File::exists($directory) && is_dir($directory)) {
            $files = File::files($directory);

            return count($files);
        }

        return null;
    }

    public static function isDefaultLogo (string $logoPath): bool
    {
        $logoPathArray = explode("/", $logoPath);
        return isset($logoPathArray[0]) && $logoPathArray[0] == self::DEFAULT_IMAGES_FOLDER;
    }
}
