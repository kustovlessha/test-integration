<?php

namespace App\Hydrators;

class MainPageHydrator
{
    public function buildCategoryTree (?array $categories): ?array
    {
        if (!$categories) {
            return null;
        }

        // Sort categories by id DESC
        $categoriesWithKeys = collect($categories)->keyBy('id')->toArray();
        krsort($categoriesWithKeys);

        $result = [];

        foreach ($categoriesWithKeys as $key => $category) {
            // $categoriesWithKeys[$key] а не $category, так как в $category не учтены изменения в ходе данного цыкла
            if ($category['parent_id']) {
                $categoriesWithKeys[$category['parent_id']]['childs'][] = $categoriesWithKeys[$key];
            } else {
                $result[] = $categoriesWithKeys[$key];
            }
        }

        return $result;
    }
}
