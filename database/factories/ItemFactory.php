<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'shop_id' => rand(1, 2),
            'type' => 1,
            'category_id' => rand(1, 6),
            'name' => fake()->sentence(),
            'description' => fake()->text(),
            'logo' => Str::random(10),
            'usdc_price' => rand(1, 6),
            'coinbase_checkout_id' => Str::random(10),
            'file_link' => Str::random(10),
        ];
    }
}
