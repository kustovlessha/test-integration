<?php

namespace Database\Seeders;

use App\Models\ItemCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ItemCategorySeeder extends Seeder
{
    /*
     * Для заполнения БД категориями для электронных товаров
     *
     * php artisan db:seed --class=ItemCategorySeeder
     * */
    public function run(): void
    {
        $errors = [];
        $categories = [
            ['id' => 1, 'name' => 'Электронные товары'],

            ['id' => 2, 'name' => 'Электронные книги', 'parent_id' => 1],
            ['id' => 3, 'name' => 'Фантастика', 'parent_id' => 2],
            ['id' => 4, 'name' => 'Детективы и триллеры', 'parent_id' => 2],
            ['id' => 5, 'name' => 'Романы', 'parent_id' => 2],
            ['id' => 6, 'name' => 'Научная литература', 'parent_id' => 2],
            ['id' => 7, 'name' => 'Деловая литература', 'parent_id' => 2],
            ['id' => 8, 'name' => 'Детская литература', 'parent_id' => 2],
            ['id' => 9, 'name' => 'Образовательные книги', 'parent_id' => 2],
            ['id' => 10, 'name' => 'Другие жанры', 'parent_id' => 2],

            ['id' => 11, 'name' => 'Видео курсы', 'parent_id' => 1],
            ['id' => 12, 'name' => 'Программирование и IT', 'parent_id' => 11],
            ['id' => 13, 'name' => 'Дизайн и графика', 'parent_id' => 11],
            ['id' => 14, 'name' => 'Маркетинг и бизнес', 'parent_id' => 11],
            ['id' => 15, 'name' => 'Языки и лингвистика', 'parent_id' => 11],
            ['id' => 16, 'name' => 'Здоровье и фитнес', 'parent_id' => 11],
            ['id' => 17, 'name' => 'Искусство и музыка', 'parent_id' => 11],
            ['id' => 18, 'name' => 'Личное развитие', 'parent_id' => 11],
            ['id' => 19, 'name' => 'Другие области', 'parent_id' => 11],

            ['id' => 20, 'name' => 'Аудиокниги', 'parent_id' => 1],
            ['id' => 21, 'name' => 'Художественные произведения', 'parent_id' => 20],
            ['id' => 22, 'name' => 'Образовательные материалы', 'parent_id' => 20],
            ['id' => 23, 'name' => 'Детские аудиокниги', 'parent_id' => 20],
            ['id' => 24, 'name' => 'Бизнес и саморазвитие', 'parent_id' => 20],
            ['id' => 25, 'name' => 'Религия и духовность', 'parent_id' => 20],
            ['id' => 26, 'name' => 'Другие жанры', 'parent_id' => 20],

            ['id' => 27, 'name' => 'Онлайн уроки', 'parent_id' => 1],
            ['id' => 28, 'name' => 'Языки', 'parent_id' => 27],
            ['id' => 29, 'name' => 'Математика и наука', 'parent_id' => 27],
            ['id' => 30, 'name' => 'Искусства и ремесла', 'parent_id' => 27],
            ['id' => 31, 'name' => 'Музыка и танцы', 'parent_id' => 27],
            ['id' => 32, 'name' => 'Программирование', 'parent_id' => 27],
            ['id' => 33, 'name' => 'Обучение профессиональным навыкам', 'parent_id' => 27],
            ['id' => 34, 'name' => 'Другие предметы', 'parent_id' => 27],

            ['id' => 35, 'name' => 'Онлайн консультации', 'parent_id' => 1],
            ['id' => 36, 'name' => ' Карьерное развитие', 'parent_id' => 35],
            ['id' => 37, 'name' => 'Психология и коучинг', 'parent_id' => 35],
            ['id' => 38, 'name' => 'Здоровье и фитнес', 'parent_id' => 35],
            ['id' => 39, 'name' => 'Бизнес консультирование', 'parent_id' => 35],
            ['id' => 40, 'name' => 'Юридические консультации', 'parent_id' => 35],
            ['id' => 41, 'name' => 'Техническая поддержка', 'parent_id' => 35],
            ['id' => 42, 'name' => 'Другие области', 'parent_id' => 35],

            ['id' => 43, 'name' => 'Программное обеспечение и приложения', 'parent_id' => 1],
            ['id' => 44, 'name' => 'Офисные программы', 'parent_id' => 43],
            ['id' => 45, 'name' => 'Графические редакторы', 'parent_id' => 43],
            ['id' => 46, 'name' => 'Антивирусное ПО', 'parent_id' => 43],
            ['id' => 47, 'name' => 'Инструменты для разработки', 'parent_id' => 43],
            ['id' => 48, 'name' => 'Образовательное программное обеспечение', 'parent_id' => 43],
            ['id' => 49, 'name' => 'Игры и развлечения', 'parent_id' => 43],

            ['id' => 50, 'name' => 'Искусственный интеллект и машинное обучение', 'parent_id' => 1],
            ['id' => 51, 'name' => 'Курсы по машинному обучению', 'parent_id' => 50],
            ['id' => 52, 'name' => 'Ресурсы для исследователей', 'parent_id' => 50],
            ['id' => 53, 'name' => 'Инструменты и библиотеки', 'parent_id' => 50],
            ['id' => 54, 'name' => 'Проекты и примеры использования', 'parent_id' => 50],

            ['id' => 55, 'name' => 'Графика и дизайн', 'parent_id' => 1],
            ['id' => 56, 'name' => 'Графический дизайн', 'parent_id' => 55],
            ['id' => 57, 'name' => 'Веб-дизайн', 'parent_id' => 55],
            ['id' => 58, 'name' => 'Анимация и видеомонтаж', 'parent_id' => 55],
            ['id' => 59, 'name' => '3D-моделирование', 'parent_id' => 55],
            ['id' => 60, 'name' => 'Фотография', 'parent_id' => 55],

            ['id' => 61, 'name' => 'Музыка и звуковые эффекты', 'parent_id' => 1],
            ['id' => 62, 'name' => 'Аудиоредакторы и инструменты', 'parent_id' => 61],
            ['id' => 63, 'name' => 'Звуковые эффекты', 'parent_id' => 61],
            ['id' => 64, 'name' => 'Музыкальные инструменты', 'parent_id' => 61],
            ['id' => 65, 'name' => 'Уроки по музыке', 'parent_id' => 61],

            ['id' => 66, 'name' => 'Цифровое искусство', 'parent_id' => 1],
            ['id' => 67, 'name' => 'Иллюстрации и рисунки', 'parent_id' => 66],
            ['id' => 68, 'name' => 'Комиксы и манга', 'parent_id' => 66],
            ['id' => 69, 'name' => 'Цифровая живопись', 'parent_id' => 66],
            ['id' => 70, 'name' => 'Графические ресурсы', 'parent_id' => 66],

            ['id' => 71, 'name' => 'Электронные шаблоны и ресурсы', 'parent_id' => 1],
            ['id' => 72, 'name' => 'Шаблоны для веб-сайтов', 'parent_id' => 71],
            ['id' => 73, 'name' => 'Графические шаблоны', 'parent_id' => 71],
            ['id' => 74, 'name' => 'Презентации и отчеты', 'parent_id' => 71],
            ['id' => 75, 'name' => 'Электронные формы и документы', 'parent_id' => 71],
        ];

        foreach ($categories as $categoryData) {
            if (!ItemCategory::create($categoryData)) {
                $errors[] = $categoryData['id'];
            }
        }

        if (count($errors)) {
            print("\nCan not save categories with ids: " . implode(", ", $errors) . "\n");
        }
    }
}
