<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shop_id');
            $table->unsignedTinyInteger('type')->comment("0 - standart (физические товары); 1 - electronic");
            $table->unsignedBigInteger('category_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('logo');
            $table->decimal('usdc_price', 10, 2);
            $table->string('coinbase_checkout_id')->unique()->comment("Для формирования платёжной формы");
            $table->string('file_link')->comment("Ссылка на файл товара (для электронных товаров)");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('items');
    }
};
