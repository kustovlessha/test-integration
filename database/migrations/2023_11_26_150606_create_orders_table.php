<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shop_id');
            $table->unsignedBigInteger('item_id');
            $table->string('customer_email');
            $table->unsignedTinyInteger('status')
                ->comment("0 - new; 1 - success; 2 - fail; 3 - pending");
            $table->decimal('usdc_price', 10, 2);
            $table->timestamps();

            // Внешний ключ для связи с таблицей магазинов
            $table->foreign('shop_id')->references('id')->on('shops');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
