<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('coinbase_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_id')->comment("ID in Better World");
            $table->string('customer_email')->comment("It needs for identification buyer this item");
            $table->string('usdc_pricing')->comment("Item price in usdc");
            $table->string('event_id');
            $table->string('event_type');
            $table->string('resource');
            $table->string('code');
            $table->string('name');
            $table->string('coinbase_checkout_id');
            $table->string('coinbase_created_at');
            $table->string('coinbase_expires_at');
            $table->json('timeline')->comment("массив транзакций (со статусами)");
            $table->string('pricing_type')->comment("фиксированая/не фиксированая");
            $table->tinyInteger('done')
                ->nullable()
                ->comment("Если 1 - значит заказ по транзакции уже обработан и теперь эту транзакцию при обработке новых заказов игнорируем (на случай если покупка у того-же продавца другого товара на такую же сумму. т.к. передать в coinbase номер заказа или id товара мы не можем)");
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('coinbase_transactions');
    }
};
