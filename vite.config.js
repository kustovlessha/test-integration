import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
                'resources/js/items/checkout.js',
                'resources/js/account/settings.js',
                'resources/js/shop/save.js',
                'resources/js/seller-items/save.js',
            ],
            refresh: true,
        }),
    ],
});
