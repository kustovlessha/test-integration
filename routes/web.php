<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('web')->group(function () {
    Route::get('/', [App\Http\Controllers\MainPageController::class, 'index'])
        ->name('main.page');

    Route::get('/catalog/{category:id?}', [App\Http\Controllers\CatalogController::class, 'index'])
        ->name('catalog.index');

    Route::get('/item/{item}', [App\Http\Controllers\ItemsController::class, 'show'])->name('items.show');

    Auth::routes();
});

Route::middleware(['web', 'csrf'])->group(function () {
    // Ваши маршруты, которые будут защищены CSRF
    Route::middleware(['auth'])->group(function () {
        /*
        * User
        **/
        Route::get(
            '/home',
            [App\Http\Controllers\AccountController::class, 'index']
        )->name('home');

        Route::get(
            '/settings',
            [App\Http\Controllers\AccountController::class, 'settings']
        )->name('account.settings');

        Route::post(
            '/change-password',
            [App\Http\Controllers\AccountController::class, 'changePassword']
        )->name('change.password');

        Route::put(
            '/user/update',
            [App\Http\Controllers\AccountController::class, 'updateUser']
        )->name('user.update');

        Route::put(
            '/user/status-update',
            [App\Http\Controllers\AccountController::class, 'updateUserStatus']
        )->name('user.status-update');

        /*
         * Shop
         * */
        Route::resource('shops', App\Http\Controllers\ShopController::class);

        /*
         * Seller Items
         * */
        Route::get(
            'seller-items/{shopId}',
            [App\Http\Controllers\SellerItemsController::class, 'index']
        )->name('seller.items.index');
        Route::get(
            '/seller-item-create/{shopId}',
            [App\Http\Controllers\SellerItemsController::class, 'create']
        )->name('seller-item.create');
        Route::get(
            'seller-item-edit/{item}',
            [App\Http\Controllers\SellerItemsController::class, 'edit']
        )->name('seller-item.edit');
        Route::post(
            'seller-item-store',
            [App\Http\Controllers\SellerItemsController::class, 'store']
        )->name('seller-item.store');
        Route::put(
            'seller-item-update/{item}',
            [App\Http\Controllers\SellerItemsController::class, 'update']
        )->name('seller-item.update');
        Route::delete(
            'seller-item-destroy/{item}',
            [App\Http\Controllers\SellerItemsController::class, 'destroy']
        )->name('seller-item.destroy');
    });

    Route::post(
        '/order/create',
        [App\Http\Controllers\OrdersController::class, 'create']
    )->name('orderCreate');

    Route::post(
        '/order/check-payment',
        [App\Http\Controllers\OrdersController::class, 'checkPayment']
    )->name('orderCheckPayment');
});
