<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/fondy-payment-notifications', [App\Http\Controllers\TestController::class, 'fondyPaymentNotifications'])->name('fondyPaymentNotifications');
Route::post(
    '/coinbase-payment-notifications',
    [App\Http\Controllers\Crypto\CoinbaseController::class, 'coinbasePaymentNotifications']
)->name('coinbasePaymentNotifications');

/**
 * API for "Better world" project
 * */

Route::get(
    '/check-shop',
    [App\Http\Controllers\Api\ShopController::class, 'checkById']
)->name('api.checkShop');
Route::get(
    '/check-item',
    [App\Http\Controllers\Api\ItemsController::class, 'checkById']
)->name('api.checkItem');
Route::post(
    '/check-items-activity',
    [App\Http\Controllers\Api\ItemsController::class, 'checkItemsActivity']
)->name('api.checkItemsActivity');

/**
 * ==== /API for "Better world" project ====
 */
