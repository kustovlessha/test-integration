up:
	docker-compose up -d

down:
	docker-compose down

sh:
	docker-compose exec laravel sh
