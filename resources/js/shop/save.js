import appFunctions from '../app';
const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
const headersData = {
    headers: {
        'X-CSRF-TOKEN': csrfToken,
        'Content-Type': 'multipart/form-data'
    }
};

const shopListLink = '/shops';

const saveShop = document.getElementById('saveShopData');
const updateShop = document.getElementById('updateShopData');

if (saveShop) {
    saveShop.addEventListener('submit', function(event) {
        event.preventDefault();
        saveData(saveShop, true);
    });
}

if (updateShop) {
    updateShop.addEventListener('submit', function(event) {
        event.preventDefault();
        saveData(updateShop, false);
    });
}

function saveData(form, needRedirect) {
    let formData = new FormData(form);
    let apiUrl = form.action;

    if(isValidForm(form)) {
        if (needRedirect) {
            appFunctions.axiosSaveFormData(apiUrl, formData, headersData, form, shopListLink);
        } else {
            appFunctions.axiosSaveFormData(apiUrl, formData, headersData, form);
        }
    }
}

function isValidForm(form) {
    let logo = form.querySelector('#logo');
    let errorDiv = form.querySelector('#logo_error');

    clearErrors(form);

    // Проверяем размер файла
    if (logo.files.length > 0 && logo.files[0].size > 1024 * 1024) {
        // Show error
        logo.classList.add('is-invalid');
        errorDiv.innerHTML = 'Изображение должно быть не более 1MB';

        return false;
    }

    // Проверяем тип файла
    if (logo.files.length > 0) {
        const allowedTypes = ['image/jpeg', 'image/jpg', 'image/png'];

        if (!allowedTypes.includes(logo.files[0].type)) {
            // Show error
            logo.classList.add('is-invalid');
            errorDiv.innerHTML = 'Допустимы лишь следующие форматы изображения: jpeg,png,jpg.';

            return false;
        }
    }

    return true;
}

function clearErrors(form) {
    // Очищаем сообщения об ошибках и убираем классы Bootstrap
    let errorDivs = form.querySelectorAll('.invalid-feedback');
    errorDivs.forEach(function (errorDiv) {
        errorDiv.innerHTML = '';
    });

    let inputFields = form.querySelectorAll('.is-invalid');
    inputFields.forEach(function (inputField) {
        inputField.classList.remove('is-invalid');
    });
}

const logoField = document.getElementById('logo');
if (logoField) {
    logoField.addEventListener('change', function(event) {
        previewImage();
    });
}

function previewImage() {
    const logoField = document.getElementById('logo');
    const imagePreview = document.getElementById('logoPreview');

    // Очищаем контейнер перед отображением нового изображения
    imagePreview.innerHTML = '';

    // Создаем элемент для иконки с крестиком
    const clearIcon = document.createElement('button');
    clearIcon.type = 'button';
    clearIcon.className = 'btn-close image-close-button';
    clearIcon.addEventListener('click', function() {
        // Очищаем поле с изображением при клике на иконку
        logoField.value = '';
        // Очищаем контейнер с изображением
        imagePreview.innerHTML = '';
    });

    // Проверяем, выбран ли файл
    if (logoField.files && logoField.files[0]) {
        const reader = new FileReader();

        // Событие загрузки изображения
        reader.onload = function (e) {
            // Создаем элемент изображения и добавляем его в контейнер
            const img = document.createElement('img');
            img.src = e.target.result;
            img.style.width = '120px'; // Устанавливаем размер изображения по вашему выбору
            imagePreview.appendChild(img);
            imagePreview.appendChild(clearIcon);
        };

        // Читаем данные изображения
        reader.readAsDataURL(logoField.files[0]);
    }
}

// If exists on upload page after load page
const clearIconButton = document.getElementById('clearIconButton');
if (clearIconButton) {
    clearIconButton.addEventListener('click', function() {
        const logoField = document.getElementById('logo');
        const imagePreview = document.getElementById('logoPreview');

        // Очищаем поле с изображением при клике на иконку
        logoField.value = '';
        // Очищаем контейнер с изображением
        imagePreview.innerHTML = '';
    });
}
