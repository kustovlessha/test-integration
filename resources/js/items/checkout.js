const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
const headersData = {
    headers: {
        'X-CSRF-TOKEN': csrfToken
    }
};

const createOrderButton = document.getElementById('create-order');

let orderId = null;

createOrderButton.addEventListener('click', function() {
    let email = document.querySelector('#email-input').value;

    createOrder(itemId, email);
});

function createOrder(itemId, email)
{
    let apiUrl = '/order/create';
    let requestData = {
        email: email,
        itemId: itemId
    };

    axios.post(apiUrl, requestData, headersData)
        .then(response => {
            orderId = response.data.order_id;

            document.getElementById('pay-button').click();
            document.getElementById('close-modal').click();

            let emailErrorDiv = document.getElementById('email-validation');
            let emailErrorInput = document.getElementById('email-input');
            emailErrorDiv.innerText = "";
            emailErrorInput.classList.remove('is-invalid');

            document.querySelector('.loader-block').style.display = 'block';
            document.querySelector('#loader-msg').innerText = 'Загрузка платежной формы...';
            checkIfCloseLoader();
        })
        .catch(error => {
            let errorData = error.response.data.error;

            if (errorData.hasOwnProperty('email')) {
                let errorMsg = errorData.email[0];
                let emailErrorDiv = document.getElementById('email-validation');
                let emailErrorInput = document.getElementById('email-input');

                emailErrorInput.classList.add('is-invalid');
                emailErrorDiv.innerText = errorMsg;
            }
        });
}

function checkIfCloseLoader()
{
    let paymentContainer = document.querySelector('.bwc-modal-container');

    if (paymentContainer !== null) {
        setTimeout(function () {
            checkIfCloseLoader();
        }, 500);
    } else {
        document.querySelector('.loader-block').style.display = 'none';
        document.querySelector('#loader-msg').innerText = '';

        pendingPayment();
    }
}

function pendingPayment()
{
    const apiUrl = "/order/check-payment";
    const requestData = {
        orderId: orderId
    };
    const itemFileLink =  document.querySelector('#item-file-link');

    document.querySelector('.loader-block').style.display = 'block';
    document.querySelector('#loader-msg').innerText = 'Ожидаем подтверждения оплаты';

    axios.post(apiUrl, requestData, headersData)
        .then(response => {
            let orderStatus = response.data.orderStatus;
            let fileLink = response.data.fileLink;

            if (orderStatus === 1) {
                let linkElement = document.createElement('a');
                linkElement.href = fileLink;
                linkElement.textContent = 'Ссылка на файл';
                linkElement.classList.add('btn', 'btn-success');
                linkElement.target = '_blank';
                itemFileLink.appendChild(linkElement);
                document.querySelector('.loader-block').style.display = 'none';
                document.querySelector('#loader-msg').innerText = '';
            } else if (orderStatus === 2) {
                itemFileLink.innerText = "К сожалению, оплата не была выполнена.";
                document.querySelector('.loader-block').style.display = 'none';
                document.querySelector('#loader-msg').innerText = '';
            } else {
                setTimeout(function () {
                    pendingPayment();
                }, 5000);
            }
        })
        .catch(error => {
            let errorData = error.response.data.error;
            console.log(errorData);

            itemFileLink.innerText = "К сожалению, роизошла техническая ошибка. Обратитесь, пожалуйста, в службу поддержки.";
        });
}
