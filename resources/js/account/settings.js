import appFunctions from '../app';
const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
const headersData = {
    headers: {
        'X-CSRF-TOKEN': csrfToken
    }
};

document.getElementById('userDataForm').addEventListener('submit', function(event) {
    event.preventDefault();
    saveData('userDataForm');
});

const userStatusForm = document.getElementById('userStatusForm');
if (userStatusForm) {
    userStatusForm.addEventListener('submit', function(event) {
        event.preventDefault();
        saveData('userStatusForm');
    });
}

function saveData(formId) {
    const form = document.getElementById(formId);
    const formData = new FormData(form);
    const apiUrl = form.action;

    appFunctions.axiosSaveFormData(apiUrl, formData, headersData, form);
}
