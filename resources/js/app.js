import './bootstrap';

// Alert flash messages
if (typeof successMessage !== 'undefined') {
    toastr.success(successMessage, "Success!", {timeOut: 12000});
}
if (typeof errorMessage !== 'undefined') {
    toastr.error(errorMessage, "Error!", {timeOut: 12000});
}

class AppFunctions
{
    clearErrors(form) {
        // Очищаем сообщения об ошибках и убираем классы Bootstrap
        let errorDivs = form.querySelectorAll('.invalid-feedback');
        errorDivs.forEach(function (errorDiv) {
            errorDiv.innerHTML = '';
        });

        let inputFields = form.querySelectorAll('.is-invalid');
        inputFields.forEach(function (inputField) {
            inputField.classList.remove('is-invalid');
        });
    }

    displayErrors(errors, form) {
        // Очищаем предыдущие сообщения об ошибках
        this.clearErrors(form);

        // Перебираем ошибки и отображаем их
        Object.keys(errors).forEach(function (field) {
            let inputField = form.querySelector(`#${field}`);
            let errorDiv = form.querySelector(`#${field}_error`);

            if (inputField && errorDiv) {
                // Добавляем класс Bootstrap для подсветки ошибок
                inputField.classList.add('is-invalid');
                // Отображаем сообщение об ошибке
                errorDiv.innerHTML = errors[field][0];
            }
        });
    }

    axiosSaveFormData(apiUrl, formData, headersData, form, redirectLink= null) {
        axios.post(apiUrl, formData, headersData)
            .then((response) => {
                this.clearErrors(form);

                if (redirectLink) {
                    window.location.href = redirectLink;
                } else {
                    toastr.success(response.data.message, "Success!", {timeOut: 12000});
                }
            })
            .catch((error) => {
                if (error.response) {
                    if (typeof error.response.data.message !== 'undefined' && error.response.data.message.includes('CSRF token mismatch')) {
                        toastr.error("Страница длительное время находилась без активности. Перезагрузите данную страницу.", "Error!", {timeOut: 12000});
                    }

                    this.displayErrors(error.response.data.errors, form);

                } else {
                    toastr.error("Не удалось сохранить данные", "Error!", {timeOut: 12000});
                    console.error('Request failed:', error.message);
                }
            });
    }
}

const appFunctions = new AppFunctions();

export default appFunctions;
