@extends('layouts.app')
@section('title', $item->name)

@section('header')
    <h1>{{ $item->name }}</h1>
@endsection

@section('content')
    <p>{{ $item->description }}</p>
    <p>Price: ${{ $item->usdc_price }}</p>

    <!-- Платёжная кнопка -->
    <div>
        <a class="buy-with-crypto" id="pay-button" style="display: none"
           href="https://commerce.coinbase.com/checkout/{{ $item->coinbase_checkout_id }}">
            Buy в криптовалюте
        </a>
        <script src="https://commerce.coinbase.com/v1/checkout.js?version=201807"></script>
    </div>
    <!-- /Платёжная кнопка -->

    <!-- Модальное окно -->
    <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalToggleLabel">
                        Buying <b>{{ $item->name }}</b> <span class="text-success">${{ $item->usdc_price }}</span>
                    </h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                </div>
                <div class="modal-body">
                    <p class="text-danger">Обязательно введите e-mail сейчас, тот же, что и при оформлении оплаты! Иначе вы не сможете получить данный товар!</p>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input type="email" class="form-control" id="email-input" aria-describedby="emailHelp">
                        <div id="email-validation" class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="create-order">Buy item</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /Модальное окно -->

    <a class="btn btn-primary" data-bs-toggle="modal" href="#exampleModalToggle" role="button">Buy</a>

    <p id="item-file-link" class="mt-2 text-danger h4"></p>

    <!-- Лоадер -->
    <div class="loader-block">
        <div class="loader-pack"><div class="spinner-border loader" role="status"></div></div>
        <div class="loader-msg" id="loader-msg"></div>
    </div>
    <!-- /Лоадер -->

@endsection

@push('scripts')
    <script>
        let itemId = @json($item->id);
    </script>

    @vite('resources/js/items/checkout.js')
@endpush
