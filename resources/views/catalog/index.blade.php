@extends('layouts.app')
@section('title', 'Shop items')

@section('content')
    <main class="mt-5 mt-lg-5">
        <h1>{{ $category->name }}</h1>

        <section class="mt-5 mt-lg-5">
            <h2 class="fs-5 fw-bold">Фильтры</h2>

            @include('catalog.partials.filters-form')
        </section>

        <section class="mt-5 mt-lg-5">
            <h2 class="fs-5 fw-bold">Товары</h2>

            <div>
                @each('catalog.partials.product-card', $items, 'item', 'catalog.partials.product-card-empty')
            </div>

            <div>
                Найдено товаров: {{ $items->total() }}
            </div>

            <div class="pagination justify-content-center">
                {!! $items->withQueryString()->links('vendor.pagination.bootstrap-5') !!}
            </div>
        </section>

        <section class="mt-5 mt-lg-5">
            <h2 class="fs-5 fw-bold">Категории</h2>

            <div>
                <ul class="categories-menu">
                    @each('catalog.partials.category-element', $categories, 'category', 'catalog.partials.category-element-empty')
                </ul>
            </div>
        </section>
    </main>
@endsection
