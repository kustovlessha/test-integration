<li>
    <a href="{{ route('catalog.index', $category['id']) }}">{{ $category['name'] }}</a>

    @if(isset($category['childs']))
        <ul class="categories-submenu">
            @each('catalog.partials.category-element', $category['childs'], 'category')
        </ul>
    @endif
</li>
