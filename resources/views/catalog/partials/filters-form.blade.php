<form action="{{ route('catalog.index', $categoryId) }}" method="get">
    <div>
        <label for="search">Поиск:</label>
        <input type="text" name="search" id="search" value="{{ $search ?? '' }}" placeholder="Search">
    </div>

    <div>
        <label for="price">Минимальная цена:</label>
        <input type="number" name="filters[minPrice]" id="minPrice" value="{{ $filters['minPrice'] ?? 0 }}">
    </div>

    <div>
        <label for="price">Максимальная цена:</label>
        <input type="number" name="filters[maxPrice]" id="maxPrice" value="{{ $filters['maxPrice'] ?? $maxPrice }}">
    </div>

    <div>
        <label for="sort">Сортировать по:</label>
        <select name="sort" id="sort">
            <option value="default" {{ empty($sortField) || $sortField === 'default' ? 'selected' : '' }}>По умолчанию</option>
            <option value="name" {{ $sortField === 'name' ? 'selected' : '' }}>Имя</option>
            <option value="priceDown" {{ $sortField === 'priceDown' ? 'selected' : '' }}>От дорогих к дешевым</option>
            <option value="priceUp" {{ $sortField === 'priceUp' ? 'selected' : '' }}>От дешевых к дорогим</option>
        </select>
    </div>

    <button type="submit">Применить фильтры и сортировки</button>
</form>
