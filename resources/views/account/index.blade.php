@extends('layouts.app')
@section('title', $user->name)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __($user->name) }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <p>Ваш ID: <span id="textToCopy">{{ __($user->id) }}</span> <button onclick="copyText()" type="button" class="btn btn-success">Copy</button></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function copyText() {
            let textElement = document.getElementById('textToCopy');
            let textToCopy = textElement.innerText;

            navigator.clipboard.writeText(textToCopy)
                .then(function() {
                    alert('Текст скопирован!');
                })
                .catch(function(err) {
                    console.error('Не удалось скопировать текст: ', err);
                });
        }
    </script>
@endpush
