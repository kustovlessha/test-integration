@extends('layouts.app')
@section('title', "Account settings " . $user->name)

@section('content')
    <div class="container">
        <!-- Редактирование личных данных -->
        <div class="row justify-content-center mb-3">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Редактирование данных пользователя</div>

                    <div class="card-body">
                        <form id="userDataForm" method="POST" action="{{ route('user.update', ['id' => $user->id]) }}">
                            @csrf
                            @method('PUT')

                            <div class="mb-3">
                                <label for="name" class="form-label">Имя</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" required>
                                <div class="invalid-feedback" id="name_error"></div>
                            </div>

                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" required>
                                <div class="invalid-feedback" id="email_error"></div>
                            </div>

                            <!-- Другие поля для редактирования -->

                            <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Обновление пароля -->
        <div class="row justify-content-center mb-3">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Изменение пароля</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('change.password') }}">
                            @csrf

                            <div class="mb-3">
                                <label for="current_password" class="form-label">Текущий пароль</label>
                                <input type="password" class="form-control" id="current_password" name="current_password" required>
                            </div>

                            <div class="mb-3">
                                <label for="new_password" class="form-label">Новый пароль</label>
                                <input type="password" class="form-control" id="new_password" name="new_password" required>
                            </div>

                            <div class="mb-3">
                                <label for="new_password_confirmation" class="form-label">Подтверждение нового пароля</label>
                                <input type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation" required>
                            </div>

                            <button type="submit" class="btn btn-primary">Изменить пароль</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Изменение статуса пользователя -->
        <div class="row justify-content-center mb-3">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Изменение статуса (применится ко всем магазинам)</div>

                    <div class="card-body">
                        @if($user->status === \App\Models\User::STATUS_BLOCKED)
                            <p>Вас заблокировали за нарушение правил платформы. Обратитесь, пожалуйста, в службу поддержки.</p>
                        @else
                            <form id="userStatusForm" method="POST" action="{{ route('user.status-update', ['id' => $user->id]) }}">
                                @csrf
                                @method('PUT')

                                <div class="mb-3">
                                    <label for="status" class="form-label">Статус продавца</label>
                                    <select class="form-select" aria-label="User status" name="status" id="status">
                                        <option
                                            {{ ($user->status === \App\Models\User::STATUS_ACTIVE) ? ' selected ' : '' }}
                                            value="{{ \App\Models\User::STATUS_ACTIVE }}">
                                            Активный
                                        </option>
                                        <option
                                            {{ ($user->status === \App\Models\User::STATUS_VACATION) ? ' selected ' : '' }}
                                            value="{{ \App\Models\User::STATUS_VACATION }}"
                                        >
                                            В отпуске (все товары не активны)
                                        </option>
                                    </select>
                                    <div class="invalid-feedback" id="status_error"></div>
                                </div>

                                <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    @vite('resources/js/account/settings.js')
@endpush
