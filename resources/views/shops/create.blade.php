@extends('layouts.app')
@section('title', 'New Store')

@section('content')
    <div class="container mt-5">
        <h2>Create Shop</h2>

        <!-- Форма создания магазина -->
        <form id="saveShopData" action="{{ route('shops.store') }}" method="POST" enctype='multipart/form-data'>
            @csrf
            @include('shops.partials.form-fields', [ 'shop' => null ])
        </form>
    </div>
@endsection

@push('scripts')
    @vite('resources/js/shop/save.js')
@endpush

