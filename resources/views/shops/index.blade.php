@extends('layouts.app')
@section('title', 'Stores')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h2>Shops List</h2>

            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <a href="{{ route('shops.create') }}" type="button" class="btn btn-success">Create</a>
            </div>

            @if(empty($shops))
                <p>У вас пока нет магазинов.</p>
            @else
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>User ID</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($shops as $shop)
                    <tr>
                        <td>{{ $shop->id }}</td>
                        <td>{{ $shop->user_id }}</td>
                        <td>{{ $shop->type === 0 ? 'Standart (Physical Goods)' : 'Electronic' }}</td>
                        <td>
                            @if($shop->status === 0)
                                Blocked
                            @elseif($shop->status === 1)
                                Active
                            @else
                                Vacation
                            @endif
                        </td>
                        <td>{{ $shop->name }}</td>
                        <td>
                            <a href="{{ route('seller.items.index', $shop->id) }}" class="btn btn-secondary">Items</a>
                            <a href="{{ route('shops.edit', $shop->id) }}" class="btn btn-warning">Edit</a>
                            <form action="{{ route('shops.destroy', $shop->id) }}" method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
@endsection
