{{--@if(empty($shop))--}}
{{--    <div class="mb-3">--}}
{{--        <label for="type" class="form-label">Type</label>--}}
{{--        <select class="form-select" id="type" name="type" required>--}}
{{--            <option value="0">Standart (Physical Goods)</option>--}}
{{--            <option value="1">Electronic</option>--}}
{{--        </select>--}}
{{--    </div>--}}
{{--@endif--}}

<div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ ($shop && $shop->name) ? $shop->name : '' }}" required>
    <div class="invalid-feedback" id="name_error"></div>
</div>

<div class="mb-3">
    <label for="logo" class="form-label">Choose Logo</label>
    <input type="file" class="form-control" id="logo" name="logo" accept="image/*">
    <div class="invalid-feedback" id="logo_error"></div>
    <div id="logoPreview">
        @if(!empty($shop->logo))
            <img src="{{ $shop->logo }}" style="width: 120px;">
            <button type="button" class="btn-close image-close-button" aria-label="Close" id="clearIconButton"></button>
        @endif
    </div>
</div>

<div class="mb-3">
    <label for="description" class="form-label">Description</label>
    <textarea class="form-control" id="description" name="description">{{ $shop ? $shop->description : '' }}</textarea>
    <div class="invalid-feedback" id="description_error"></div>
</div>

<div class="mb-3">
    <label for="contacts" class="form-label">Contacts</label>
    <textarea class="form-control" id="contacts" name="contacts">{{ $shop ? $shop->contacts : '' }}</textarea>
    <div class="invalid-feedback" id="contacts_error"></div>
</div>

<div class="mb-3">
    <label for="status" class="form-label">Status</label>
    <select class="form-select" id="status" name="status" required>
        <option value="1" {{ $shop && $shop->status === 1 ? 'selected' : '' }}>Active</option>
        <option value="2" {{ $shop && $shop->status === 2 ? 'selected' : '' }}>Vacation</option>
    </select>
    <div class="invalid-feedback" id="status_error"></div>
</div>

<button type="submit" class="btn btn-primary">
    @if(empty($shop))
        Create Shop
    @else
        Update Shop
    @endif
</button>
