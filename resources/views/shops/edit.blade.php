@extends('layouts.app')
@section('title', 'Edit Store')

@section('content')
    <div class="container mt-5">
        <h2>Edit Shop</h2>

        <!-- Форма редактирования магазина -->
        <form id="updateShopData" action="{{ route('shops.update', $shop->id) }}" method="POST" enctype='multipart/form-data'>
            @csrf
            @method('PUT')

            @include('shops.partials.form-fields')
        </form>
    </div>

@endsection

@push('scripts')
    @vite('resources/js/shop/save.js')
@endpush
