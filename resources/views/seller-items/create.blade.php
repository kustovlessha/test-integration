@extends('layouts.app')
@section('title', 'New Item')

@section('content')
    <div class="container mt-5">
        <h2>Create Item</h2>

        <!-- Форма создания товара -->
        <form id="saveItemData" action="{{ route('seller-item.store') }}" method="POST" enctype='multipart/form-data'>
            @csrf

            @if($shop->type === \App\Models\Shop::ELECTRONIC_TYPE)
                @include('seller-items.partials.form-electronic-item-fields', [ 'item' => null ])
            @elseif($shop->type === \App\Models\Shop::PHYSICAL_TYPE)
                @include('seller-items.partials.form-physical-item-fields', [ 'item' => null ])
            @endif
        </form>
    </div>
@endsection

@push('scripts')
    @vite('resources/js/seller-items/save.js')
@endpush

