@extends('layouts.app')
@section('title', 'Shop items')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h2>Shop "{{ $shop->name }}" items List</h2>

            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <a href="{{ route('seller-item.create', $shop->id) }}" type="button" class="btn btn-success">Create</a>
            </div>

            @if(empty($items))
                <p>В данном магазине пока нет товаров.</p>
            @else
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>{{ $item['id'] }}</td>
                        <td>{{ $item['name'] }}</td>
                        <td>
                            <a href="{{ route('seller-item.edit', $item['id']) }}" class="btn btn-warning">Edit</a>
                            <form action="{{ route('seller-item.destroy', $item['id']) }}" method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
@endsection
