@extends('layouts.app')
@section('title', 'Edit item')

@section('content')
    <div class="container mt-5">
        <h2>Edit Item</h2>

        <!-- Форма редактирования товара -->
        <form id="updateItemData" action="{{ route('seller-item.update', ['item' => $item->id]) }}" method="POST" enctype='multipart/form-data'>
            @csrf
            @method('PUT')

            @if($item->type === \App\Models\Item::ELECTRONIC_TYPE)
                @include('seller-items.partials.form-electronic-item-fields')
            @elseif($item->type === \App\Models\Item::STANDART_TYPE)
                @include('seller-items.partials.form-physical-item-fields')
            @endif
        </form>
    </div>

@endsection

@push('scripts')
    @vite('resources/js/seller-items/save.js')
@endpush
