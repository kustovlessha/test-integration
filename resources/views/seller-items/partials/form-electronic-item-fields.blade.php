@if(!$item)
    <input type="hidden" name="shop_id" value="{{ $shop->id }}">
    <input type="hidden" name="type" value="{{ $shop->type }}">
@endif

<!-- TODO -->
<input type="hidden" name="category_id" value="1">

<div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ ($item && $item->name) ? $item->name : '' }}" required>
    <div class="invalid-feedback" id="name_error"></div>
</div>

<div class="mb-3">
    <label for="usdc_price" class="form-label">Price in USDC</label>
    <input type="text" class="form-control" id="usdc_price" name="usdc_price" value="{{ ($item && $item->usdc_price) ? $item->usdc_price : '' }}" required>
    <div class="invalid-feedback" id="usdc_price_error"></div>
</div>

<div class="mb-3">
    <label for="logo" class="form-label">Choose Logo</label>
    <input type="file" class="form-control" id="logo" name="logo" accept="image/*">
    <div class="invalid-feedback" id="logo_error"></div>
    <div id="logoPreview">
        @if(!empty($item->logo))
            <img src="{{ $item->logo }}" style="width: 120px;">
            <button type="button" class="btn-close image-close-button" aria-label="Close" id="clearIconButton"></button>
        @endif
    </div>
</div>

<div class="mb-3">
    <label for="coinbase_checkout_id" class="form-label">Coinbase checkout id</label>
    <input type="text" class="form-control" id="coinbase_checkout_id" name="coinbase_checkout_id" value="{{ ($item && $item->coinbase_checkout_id) ? $item->usdc_price : '' }}" required>
    <div class="invalid-feedback" id="coinbase_checkout_id_error"></div>
</div>

<div class="mb-3">
    <label for="file_link" class="form-label">File link</label>
    <input type="text" class="form-control" id="file_link" name="file_link" value="{{ ($item && $item->file_link) ? $item->file_link : '' }}" required>
    <div class="invalid-feedback" id="file_link_error"></div>
</div>

<div class="mb-3">
    <label for="description" class="form-label">Item description</label>
    <textarea class="form-control" id="description" name="description">{{ $item ? $item->description : '' }}</textarea>
    <div class="invalid-feedback" id="description_error"></div>
</div>

<div class="mb-3">
    <label for="status" class="form-label">Item status</label>
    @if($item)
        @if($item->status === \App\Models\Item::STATUS_BLOCKED)
            <p>Товар заблокирован за нарушение правил платформы. Обратитесь, пожалуйста, в службу поддержки.</p>
        @else
            <select class="form-select" aria-label="Item status" name="status" id="status">
                <option
                    {{ ($item->status === \App\Models\Item::STATUS_ACTIVE) ? ' selected ' : '' }}
                    value="{{ \App\Models\Item::STATUS_ACTIVE }}">
                    Активный
                </option>
                <option
                    {{ ($item->status === \App\Models\Item::STATUS_INACTIVE) ? ' selected ' : '' }}
                    value="{{ \App\Models\Item::STATUS_INACTIVE }}"
                >
                    Товара нет в наличии
                </option>
            </select>
            <div class="invalid-feedback" id="status_error"></div>
        @endif
    @endif
</div>

<button type="submit" class="btn btn-primary">
    @if(empty($item))
        Create Item
    @else
        Update Item
    @endif
</button>
