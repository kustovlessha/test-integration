@extends('layouts.app')
@section('title', 'Shop items')

@section('content')
    <main class="mt-5 mt-lg-5">
        <section class="mt-5 mt-lg-5">
            <h2 class="fs-5 fw-bold">Категории</h2>

            <div>
                <ul class="categories-menu">
                    @each('catalog.partials.category-element', $categories, 'category', 'catalog.partials.category-element-empty')
                </ul>
            </div>
        </section>

        <section class="mt-5 mt-lg-5">
            <h2 class="fs-5 fw-bold">Товары</h2>

            <div>
                @each('catalog.partials.product-card', $items, 'item', 'catalog.partials.product-card-empty')
            </div>
        </section>
    </main>
@endsection
